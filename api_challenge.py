# we need to authenticate ourselve and get the access tokens back.
# once we get the tokens, we notice that the layout of the content is same
# we can create the json for one category then try looping across
# this will probably time out but we will do optimizations
import json
import os
import requests
# authentication 
def get_access_keys(access_url, client_id, client_sec, **api_payload):

    api_payload.update({
        "client_id": client_id,
        "client_secret": client_sec
    })

    request_headers = {
        "API-Version": 'v2',
        "Accept-Language": 'en',
        "Accept": 'application/json'
    }

    api_headers = requests.post(
        url = access_url, data=api_payload, headers=request_headers
    )

    api_headers = api_headers.json()

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization" : "{} {}".format(api_headers["token_type"], api_headers["access_token"]),
        "API-Version":"v2",
        "Accept-Language": "en",
    }
    return headers

def make_hit_to_resource(headers, resource_url):
    r = requests.get(resource_url, headers=headers, verify=False)
    r.raise_for_status()
    data = r.json()
    return data

def extract_child(payload):
    child_data = payload['child']
    # the number of childre
    return child_data[0]

def write_to_file(file_name, payload):

    with open(file_name, 'w' ) as ff:
        ff.write(json.dumps(payload))




#icd10
access_tokens_url = "https://icdaccessmanagement.who.int/connect/token"
resource_url = 'https://id.who.int/icd/release/10/2019'

client_id = os.environ.get('CLIENT_ID') 
client_sec = os.environ.get('CLIENT_SECRET')

if not client_id and not client_sec:
    raise Exception('Client_id or Client_sec is None.Kindly check your env file and source it') 

api_payload={
    "scopes": "icdapi_access",
    "grant_type": "client_credentials"
}

headers = get_access_keys(access_tokens_url, client_id, client_sec, **api_payload)

parent_url_1 = make_hit_to_resource(headers, resource_url)
write_to_file('parent_1', parent_url_1)
child = extract_child(parent_url_1)
child_1 = make_hit_to_resource(headers, child)
write_to_file('classification', child_1)
child_1b = extract_child(child_1)
child_2 = make_hit_to_resource(headers, child_1b)
write_to_file('block_range', child_2)
child_1c = extract_child(child_2)

child_3 = make_hit_to_resource(headers, child_1c)
write_to_file('category', child_3)
child_1d = extract_child(child_3)
child_4 = make_hit_to_resource(headers, child_1d)
write_to_file('diagnosis', child_4)
# get_data

